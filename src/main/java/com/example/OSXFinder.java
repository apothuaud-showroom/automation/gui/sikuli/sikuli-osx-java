package com.example;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;
import org.sikuli.script.Screen;

import java.io.File;

public class OSXFinder {

    String basePath;

    public OSXFinder() {
        basePath = new File("src/main/resources/images/OSXFinder").getAbsolutePath();
    }

    public void open() throws InterruptedException {
        Screen s0 = new Screen();
        s0.type(Key.SPACE, Key.CMD);
        Thread.sleep(1000);
        s0.type(Key.BACKSPACE);
        try {
            s0.wait(basePath + "/spotlightOpenned.png");
        } catch (FindFailed findFailed) {
            try {
                s0.wait(basePath + "/spotlightOpennedBlack.png");
            } catch (FindFailed failed) {
                failed.printStackTrace();
            }
        }
    }

    public void search(String query) {
        Screen s0 = new Screen();
        s0.paste(query);
    }

    public void shouldSee(String result) throws FindFailed {
        Screen s0 = new Screen();
        String image = result.toLowerCase().replace(" ", "_") + ".png";
        s0.wait(basePath + "/" + image);
    }
}
