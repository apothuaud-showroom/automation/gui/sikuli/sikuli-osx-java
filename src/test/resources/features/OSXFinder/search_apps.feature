Feature: Search applications using OX Finder

  Scenario: Search for safari

    Given I open OSX Finder
    When I search for "safari"
    Then I should see "safari browser" on search results