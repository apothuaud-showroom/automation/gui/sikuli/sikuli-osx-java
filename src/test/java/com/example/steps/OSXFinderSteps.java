package com.example.steps;

import com.example.OSXFinder;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.sikuli.script.FindFailed;

import java.net.MalformedURLException;

public class OSXFinderSteps {

    @Given("I open OSX Finder")
    public void iOpenOSXFinder() throws InterruptedException, MalformedURLException {
        OSXFinder finder = new OSXFinder();
        finder.open();
    }

    @When("I search for {string}")
    public void iSearchFor(String query) throws MalformedURLException {
        OSXFinder finder = new OSXFinder();
        finder.search(query);
    }

    @Then("I should see {string} on search results")
    public void iShouldSeeOnSearchResults(String result) throws FindFailed, MalformedURLException {
        OSXFinder finder = new OSXFinder();
        finder.shouldSee(result);
    }
}
